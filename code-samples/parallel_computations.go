package main

import "fmt"

func multiplier(input <-chan int, amount int, output chan<- int) {
  iterations := <-input;
  for ; iterations > 0; iterations-- {
    output<-(<-input * amount);
  }
}

func adder(input <-chan int, amount int, output chan<- int) {
  iterations := <-input;
  for ; iterations > 0; iterations-- {
    output<-(<-input + amount);
  }
}

func main() {
  source := make(chan int, 10);
  multiplied := make(chan int, 10);

  result := make(chan int, 10);

  go multiplier(source, 2, multiplied);
  go adder(multiplied, 5, result);

  source<-10;
  multiplied<-10;
  for i := 1; i <= 10; i++ {
    source<-i;
  }

  for i := 0; i < 10; i++ {
    fmt.Printf("%d\n", <-result);
  }
}
