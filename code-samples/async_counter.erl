-module(main).
-export([start/0, counter/0]).

counter() -> counter(0).

counter(N) ->
  receive
    {get, From} ->
      From ! {ok, N},
      counter(N);
    advance -> counter(N + 1)
  end.

get_counter(Counter) ->
  Counter ! {get, self()},
  receive
    {ok, N} -> N;
    _ -> -1
  end.

advance_counter(Counter) -> Counter ! advance.

start() ->
  CounterPID = spawn(main, counter, []),
  io:format("~w\n", [get_counter(CounterPID)]),
  advance_counter(CounterPID),
  advance_counter(CounterPID),
  io:format("~w\n", [get_counter(CounterPID)]).
