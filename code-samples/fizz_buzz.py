def fizz_buzz_single(num):
    output = ""
    if num % 3 == 0:
        output += "Fizz"
    if num % 5 == 0:
        output += "Buzz"

    if output == "":
        output = str(num)

    return output

def fizz_buzz():
    for i in range(1, 101):
        print(fizz_buzz_single(i))

if __name__ == "__main__":
    fizz_buzz()
