fn fizz_buzz_single(num: i32) -> String {
  match (num % 5 == 0, num % 3 == 0) {
    (false, false) => num.to_string(),
    (false, true) => "Fizz".to_string(),
    (true, false) => "Buzz".to_string(),
    (true, true) => "FizzBuzz".to_string()
  }
}

fn fizz_buzz() {
  for i in 1..101 {
    println!("{}", fizz_buzz_single(i));
  }
}

fn main() {
  fizz_buzz();
}
