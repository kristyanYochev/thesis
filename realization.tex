\chapter{Програмна реализация. Архитектура и структура на системата}
\begin{figure}[h]
  \begin{tikzpicture}[align=center, node distance=2.5cm]
    \node(start)[startstop]{Начало};
    \node(read_source)[io, below of=start]{Прочитане на програмния файл};
    \node(lexer)[process, below of=read_source]{Лексикален анализ};
    \node(parser)[process, below of=lexer]{Синтактичен анализ};
    \node(resolver)[process, below of=parser]{Семантичен анализ};
    \node(interpreter)[process, below of=resolver]{Изпълнение};
    \node(end)[startstop, below of=interpreter]{Kрай};
    \node[draw, dotted, fit=(lexer) (parser) (resolver), label=right:{Статичен анализ}] {};
    
    \draw[arrow] (start) -- (read_source);
    \draw[arrow] (read_source) -- node[anchor=east]{Програмен текст} (lexer);
    \draw[arrow] (lexer) -- node[anchor=east]{Морфеми} (parser);
    \draw[arrow] (parser) -- node[anchor=east]{Абстрактни синтактични дървета} (resolver);
    \draw[arrow] (resolver) -- node[anchor=east]{Анотации} (interpreter);
    \draw[arrow] (interpreter) -- (end);
  \end{tikzpicture}
  \caption{Блок схема на проекта}
  \label{fig:general-flowchart}
\end{figure}

Системата е изградена от 4 основни компонента \textendash\  лексикален анализатор, интактичен анализатор,
семантичен анализатор и интерпретатор (фигура \ref{fig:general-flowchart}).
Колективното име за първите три е \enquote{статичен анализатор}, защото те не изпълняват кода,
а само правят проверки за правилността на програмния текст.

\section{Дефиниция на програмния език}
Целта на тази дипломна работа е да представи език, който позволява лесно създаване
на конкурентни системи. За да бъде постигната тази цел, езикът трябва да
притежава посочените по-долу качества.

\subsection{Подобие с други езици}
Синтаксисът на езика е създаден по модела на C-подобните езици. Тъй като
те са най-често ползваното семейство езици, повечето програмисти ще могат
интуитивно да използват синтаксиса, което води до по-лесното заучаване на езика.

Също така поведението на обектите е подобно на това от C-подобните езици. Това
още допринася към интуитивността на езика, дори и преди програмистът да не го е ползвал.

Идеите, които са добавени в езика и не съществуват в другите езици, са създадени
като естествено разширение на идеите от повечето езици.

\subsection{Константност по подразбиране}
Всеки идентификатор при декларация е константен, освен ако не е деклариран като
променлив с ключовата дума \texttt{mut}. Всяка функция, която променя състояние,
което не ѝ принадлежи, трябва да бъде декларирана с ключовата дума \texttt{mut}
и тя не може да бъде викана от друга функция, ако викащата функция не е декларирана
също с \texttt{mut}. Така насочваме програмиста към използване на минимален брой
променливи идентификатори в кода си, което води до по-лесна поддръжка и по-малка
възможност за допускане на грешки, особено при програмирането на конкурентни системи
\cite{thinking-outside-sync-quadrant}.

\section{Компоненти на системата}
\subsection{Лексикален анализатор}
Лескикалният анализатор чете програмния текст по 2 знака наведнъж и от него изгражда
списък с морфеми (виж \ref{tokens} - \enquote{Морфеми}). За всяка морфема той определя
реда от програмния текст, в който се намира, заедно с вида ѝ (виж таблица \ref{tab:token-types})
и извлича оригиналния ѝ текст.

Коментарите биват 2 вида \textendash\  едноредови и многоредови. Едноредов коментар започва с \texttt{//}
и завършва с края на реда. Многоредов коментар започва с \texttt{/*} и завършва с \texttt{*/}.

\begin{figure}[H]
  \begin{minted}{text}
    let a = 5; // Едноредов коментар
    /*
    Могоредов
    коментар
    */
  \end{minted}
  \caption{Пример за коментари}
  \label{fig:cflat-comments}
\end{figure}

\subsection{Синтактичен анализатор} \label{parser}
Задачата на синтактичния анализатор е да построи дървовидно представяне (виж \ref{asts})
на всяка команда в програмния код. Той имплементира правилата на безконтекстната
граматика на езика, използвайки алгоритъма на рекурсивното спускане. Всяко правило
в безконтекстанта граматика на езика (виж приложение \ref{cflat-cfg}) е представено от
съответен метод в класа \texttt{Parser}, който изгражда клон от абстрактното
синтактично дърво за командата, която бива анализирана (фигура \ref{fig:parser-example}).

\begin{figure}[H]
  \centering
  \newcommand{\token}[1]{\framebox{\texttt{#1}}}
  \begin{tikzpicture}[align=center, node distance = 10em]
    \node(plaintext)[label=below:{Програмен текст}]{\texttt{2 + 3 * 5.2}};
    \node(tokens)[right of=plaintext, label=below:{Морфеми}]{\token{2} \token{+} \token{3} \token{*} \token{5.2}};
    \node(ast)[astnode, right of=tokens, label=above:{Абстрактно синтактично дърво}]{+}
    child[astnode] {node[astnode] {2}}
    child[astnode] {node[astnode] {*}
    child[astnode] {node[astnode] {3}}
    child[astnode] {node[astnode] {5.2}}
    };
    
    \draw[arrow] (plaintext) -- (tokens);
    \draw[arrow] (tokens) -- (ast);
  \end{tikzpicture}
  \caption{Пример за работата на синтактичния анализатор}
  \label{fig:parser-example}
\end{figure}

\subsection{Семантичен анализатор}
Ако програмата достигне тази фаза, то програмният текст е правилно форматиран,
но има още възможност да се въведе грешен по смисъл код като този във фигура \ref{fig:wrong-return}.
При него има \texttt{return} команда, поставена на глобално ниво, което няма логически смисъл,
въпреки че е коректно от лексикална и синтактична гледна точка.

\begin{figure}[H]
  \begin{minted}{text}
    let myVar = 4;
    return;
  \end{minted}
  \caption{Неправилно поставена \texttt{return} команда}
  \label{fig:wrong-return}
\end{figure}

Освен грешния по смисъл код, трябва да се сложат анотации за препратките към
идентификатори в програмата, за да може интер-претаторът да знае къде да търси
стойностите, към които тези идентификатори сочат. Ако директно се прилага правилото
за най-близката среда, която съдържа идентификатора, се получава нежелано поведение
в крайни случаи. Такъв случай е този на фигура \ref{fig:wrong-binding-example}.
Ако директно приложим правилото на най-близката среда, на стандартния изход ще
се изпише \texttt{global} и \texttt{local}, което е неинтуитивно. Повечето програмисти
биха очаквали да се изпише \texttt{global} два пъти, защото повечето езици използват
статични среди. Затова се създава една таблица, в която е описано за всеки израз
на кое ниво в дървото от среди се намира стойността на идентификатора.

\begin{figure}[H]
  \begin{minted}{text}
    let variable = "global";
    {
      func showVariable() {
        print(variable);
      }
      
      showVariable();
      let variable = "local";
      showVariable();
    }
  \end{minted}
  \caption{Прилагане на правилото за най-близката среда}
  \label{fig:wrong-binding-example}
\end{figure}

\subsection{Интерпретатор}
Интерпретаторът извършва изпълнението на програмния код. Той приема абстрактното
синтактично дърво и го обхожда рекурсивно, изпълнявайки всеки клон, на който стъпи.
При възникване на грешка по време на изпълнение програмата се прекратява и се
изписва съобщение за грешката на стандартния изход за грешки. От всеки клон за израз
бива върнат обект, представляващ резултата от изпълнението на израза.

\section{Структури от данни за представяне на елементите на програмата}

\subsection{Морфеми} \label{tokens}
Морфемите са най-малката смислена единица на езика. Всяка морфема се състои от вид
(виж таблица \ref{tab:token-types}), копие на текста ѝ от програмния текст (често
наричано \enquote{лексема}), номер на реда от програмния текст, на който е намерена и име
на файла, в който е намерена. Тези морфеми се използват от синатктичния анализатор
(виж \ref{parser}), за да изгради абстрактното синтактично дърво. Безконтекстаната
граматика на езика е дефинирана спрямо видовете морфеми.

\begin{longtable}{|c | c|}
  \hline
  Вид & Описание \\
  \hline
  \endfirsthead
  
  \multicolumn{2}{c}{\tablename \thetable -- \textit{Продължение от предишната страница}} \\
  \hline
  Вид & Описание \\
  \hline
  \endhead
  \hline
  \multicolumn{2}{c}{\textit{Продължава на следващата страница}} \\
  \endfoot
  \endlastfoot
  PLUS & Знак \texttt{+} \\
  MINUS & Знак \texttt{-} \\
  STAR & Знак \texttt{*} \\
  SLASH & Знак \texttt{/} \\
  LEFT\_PAREN & Знак \texttt{(} \\
  RIGHT\_PAREN & Знак \texttt{)} \\
  LEFT\_BRACE & Знак \texttt{\{} \\
  RIGHT\_BRACE & Знак \texttt{\}} \\
  COLON & Знак \texttt{:} \\
  SEMICOLON & Знак \texttt{;} \\
  COMMA & Знак \texttt{,} \\
  DOT & Знак \texttt{.} \\
  MODULO & Знак \texttt{\%} \\
  EQUAL & Знак \texttt{=} \\
  EQUAL\_EQUAL & Знак \texttt{==} \\
  BANG & Знак \texttt{!} \\
  BANG\_EQUAL & Знак \texttt{!=} \\
  LESS & Знак \texttt{<} \\
  LESS\_EQUAL & Знак \texttt{<=} \\
  SIGNAL & Знак \texttt{<-} \\
  IGNORE & Знак \texttt{\_} \\
  GREATER & Знак \texttt{>} \\
  GREATER\_EQUAL & Знак \texttt{>=} \\
  NUMBER & Число (напр. \texttt{143.11}) \\
  IDENTIFIER & Идентификатор (напр. \texttt{\_someVar2}) \\
  STRING & Низ от знаци (напр. \texttt{"Hello, World!"}) \\
  LET & Ключова дума \texttt{let} \\
  FUNC & Ключова дума \texttt{func} \\
  MUT & Ключова дума \texttt{mut} \\
  RETURN & Ключова дума \texttt{return} \\
  IF & Ключова дума \texttt{if} \\
  ELSE & Ключова дума \texttt{else} \\
  WHILE & Ключова дума \texttt{while} \\
  FOR & Ключова дума \texttt{for} \\
  CLASS & Ключова дума \texttt{class} \\
  THIS & Ключова дума \texttt{this} \\
  SUPER & Ключова дума \texttt{super} \\
  PUBLIC & Ключова дума \texttt{public} \\
  PRIVATE & Ключова дума \texttt{private} \\
  ACTOR & Ключова дума \texttt{actor} \\
  IMPORT & Ключова дума \texttt{import} \\
  PARTIAL & Ключова дума \texttt{partial} \\
  OR & Ключова дума \texttt{or} \\
  AND & Ключова дума \texttt{and} \\
  TRUE & Ключова дума \texttt{true} \\
  FALSE & Ключова дума \texttt{false} \\
  NULL & Ключова дума \texttt{null} \\
  EOF & Край на файла \\
  \hline
  \caption{Видове морфеми}
  \label{tab:token-types}
\end{longtable}

\subsection{Абстрактни синтактични дървета} \label{asts}
Абстрактните снтактични дървета са начин за представяне на програмния
код, чиято структура отразява реда на изпълнение и е най-удобна за манипулиране. \cite{dragon-book} \cite{craftinginterpreters}
Тези дървета се изграждат така, че когато се обхождат първо по дълбочниа (depth-first search),
редът, в който обхождаме клоните, е редът, в който трябва да се изпълнят тези клони.

Разглеждайки примера на фигура \ref{fig:ast-example} \textendash\  обхождайки дървото по дълбочина,
получаваме следните стъпки:
\begin{enumerate}
  \item Умножи 3 и 2
  \item Декларирай променлив идентификатор a и му задай стойността от предишното изчисление
\end{enumerate}

\begin{figure}[H]
  \begin{tikzpicture}[align=center, node distance = 3em, sibling distance=10em]
    \node[astnode]{variable declaration}
      child[astnode] {node[astnode] {name = a}}
      child[astnode] {node[astnode] {mutable}}
      child[astnode] {node[astnode] {*}
        child[astnode] {node[astnode] {3}}
        child[astnode] {node[astnode] {2}}
      };
  \end{tikzpicture}
  \caption{Представяне на израза \texttt{let mut a = 3 * 2} като абстрактно синтактично дърво}
  \label{fig:ast-example}
\end{figure}

Класовете, с които различните клони на едно дърво се представят, могат да бъдат намерени
в Java файловете в директорията \path{src/main/java/org/elsys/kristyanYochev/cflat/ast}.

\subsection{Представяне на елементите на програмата по време на изпълнение}
\subsubsection{Среди}
Средите представляват таблици с имена и стойности. Всяка среда има указател към родителската ѝ среда.
Така те образуват дървовидна структура, която има за корен средата, която съдържа вградените идентификатори,
чиито дефиниции са на Java (като \texttt{print}, \texttt{int} и подобни). Когато се търси стойността на даден
идентификатор, се проверява първо в текущата среда, и ако не е намерен там, се обхожда дървото от среди нагоре
до стигане до корена, и ако не е намерен и там, се показва грешка на потребителя. Имплементацията на средите
се намира във файла \path{src/main/java/org/elsys/kristyanYochev/cflat/vm/Environment.java}.

\begin{figure}[H]
  \begin{tikzpicture}[
    align=center,
    level distance = 5em,
    sibling distance = 20em,
    edge from parent/.style= {draw, latex-}]
  ]
    \node{
      \begin{tabular}{|c | c|}
        \hline
        \texttt{int} & \texttt{<CFlat Native Class>} \\
        \hline
        \texttt{float} & \texttt{<CFlat Native Class>} \\
        \hline
      \end{tabular}
    }
      child{
        node{\begin{tabular}{|c | c|}
          \hline
          \texttt{Main} & \texttt{<CFlat Actor>} \\
          \hline
        \end{tabular}}
      }
      child{
        node{\begin{tabular}{|c | c|}
          \hline
          \texttt{Point} & \texttt{<CFlat Class>} \\
          \hline
        \end{tabular}}
      };
  \end{tikzpicture}
  \caption{Дървовидна структура на средите}
\end{figure}

\subsubsection{Повикваеми обекти}
Повикваемите обекти са тези, които могат да бъдат сложени в \texttt{Call} израз.
Такива обекти са функциите, класовете и актьорите. Интерфейсът им е деклариран във
файла \path{src/main/java/org/elsys/kristyanYochev/cflat/vm/CFlatCallable.java}.

\subsubsection{Инстанции}
Инстанциите са всички обекти, на които може да се вземат членове и да се объврзват
с методи. Такива са инстанциите на всички класове и актьори. Декларацията на този
инетрфейс се намира във файла \path{src/main/java/org/elsys/kristyanYochev/cflat/vm/CFlatInstance.java}.

\subsubsection{Модули}
Един модул представлява всеки файл, който е зареден в програмата, използвайки
командата \texttt{import}. В него има среда, която е глобалната среда след изпълнението
на файла. След като един модул е зареден, той бива запазен, за да не бъде изпълнен
кодът в него повторно. Кодът за модулите се намира в
\path{src/main/java/org/elsys/kristyanYochev/cflat/vm/CFlatModule.java}.
